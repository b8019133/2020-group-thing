﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{

    public static BuildManager instance;
    public GameObject[] towers;
    public int towerSelect;
    public int[] towerCosts = {50,125,200};
    private GameObject towerToBuild;
	private Node selectedNode;
	public NodeUI nodeUI;
    int towerid;


    void Awake()
    {
        instance = this;
    }
      

    public GameObject GetTowerToBuild()
    {
        return towerToBuild;
    }
    
    public void SetTowerToBuild(GameObject tower,int id)
    {
        towerid = id;
        towerToBuild = tower;

		DeselectNode();
	}

    public bool CheckCosts(GameObject tower)
    {
        int cost;
        if (tower == towers[0])
        {
            cost = towerCosts[0];
        }
        else if(tower == towers[1])
        {
            cost = towerCosts[1];
        }else
        {
            cost = towerCosts[2];
        }


        if (GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().game.money >= cost)
        {
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().ChangeMoney(-cost);
            return true;
        }

        return false;
    }
    public void GetTurretData(Vector3 pos)
    {

        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().game.turrettype[(((int)pos.x / 5) * 17) + (-(int)pos.z / 5)] = towerid;
    }

    public void SelectNode(Node node)
	{
		if (selectedNode == node)
		{
			DeselectNode();
			return;
		}
		selectedNode = node;
		towerToBuild = null;

		nodeUI.SetTarget(node);
	}

    public void DeselectNode()
	{
		selectedNode = null;
		nodeUI.Hide();
	}

    public void LoadTurretData(int x, int z, int type)
    {
        if (type == -1)
        {
            //Debug.Log("Yes");
            return;
        }

        GameObject[] nodes = GameObject.FindGameObjectsWithTag("Node");
        //Debug.Log("Yes");
        foreach (GameObject node in nodes)
        {

            if ((node.transform.localPosition.x == x * 5) && (node.transform.localPosition.z == -z * 5))
            {
                node.GetComponent<Node>().BuildLoadedTower(towers[type]);

                break;
            }
        }

    }

}
