﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    Button btn_play, btn_loadGame, btn_settings, btn_quit;
    public GameObject play, loadgame, settings, quit;//, gameManager;
    private GameManager gm;   
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        gm.GoToMenu();
        btn_play = play.GetComponent<Button>();
        btn_loadGame = loadgame.GetComponent<Button>();
        btn_settings = settings.GetComponent<Button>();
        btn_quit = quit.GetComponent<Button>();

        btn_play.onClick.AddListener(PlayGame);
        btn_loadGame.onClick.AddListener(LoadGame);
        btn_settings.onClick.AddListener(Setting);
        btn_quit.onClick.AddListener(QuitGame);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void PlayGame()
    {
        gm.StartGame();
    }
    private void LoadGame()
    {
        gm.LoadGame();
    }
    private void Setting()
    {

    }
    private void QuitGame()
    {
    Application.Quit();
    }

}
