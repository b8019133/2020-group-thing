﻿using UnityEngine;

public class scrLookAtCamera : MonoBehaviour
{
  [SerializeField]
  [Tooltip("Leave empty to look at main camera")]
  private Transform Target;
  private Vector3 TargetPosition;
  private Vector3 barPosition;

  void Start()
  {
    Target = Camera.main.transform;
  }

  private void Update()
  {
    transform.LookAt(Target);
  }
}