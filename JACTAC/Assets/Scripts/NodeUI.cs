﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeUI : MonoBehaviour
{
    private Node target;
    public GameObject ui;
    public GameObject upgradeParticles, destroyParticles;

    public void SetTarget(Node _target)
    {
        target = _target;
        transform.position = target.transform.position + new Vector3(0,1f,0);

        ui.SetActive(true);

    }

    public void Hide()
    {
        ui.SetActive(false);
    }

    public void UpgradeTower()
    {

        if (GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().game.money >= 100)
        {
            target.tower.GetComponent<RankUp>().ModelSwitch();
            GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().ChangeMoney(-100);
            Instantiate(upgradeParticles, target.tower.transform.position, Quaternion.identity);
            Hide();
        }

        BuildManager.instance.DeselectNode();

        Hide();
    }

    public void SellTower()
    {
        GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().ChangeMoney(+target.tower.GetComponentInChildren<BaseTower>().cost / 2);
        Instantiate(destroyParticles, target.tower.transform.position, Quaternion.identity);

        Destroy(target.tower, 0f);
        target.tower = null;

        BuildManager.instance.DeselectNode();
        Hide();
    }

}
