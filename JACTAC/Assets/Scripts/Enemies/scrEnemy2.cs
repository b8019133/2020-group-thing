﻿using UnityEngine;
using UnityEngine.UI;

public class scrEnemy2 : MonoBehaviour {
  public float speed = 10f; // Speed set to 10 - can be adjusted through Unity in runtime for demonstration
  public float startHealth = 100; // Health set to 100 - can be adjusted through Unity in runtime for demonstration
  private float health;
  private Transform target;
  private int waypointIndex = 0; // Index pointer for the waypoint targetting
  public Image healthBar;
  void Start() {
    health = startHealth;
    target = scrWaypoints.waypoints[0]; // Set target to the first waypoint
  }
  public void TakeDamage(float amount) {
    health -= amount;
    healthBar.fillAmount = health / startHealth;
    if (health <= 0)
      Die();
  }
  void Update() {
    Vector3 dir = target.position - transform.position;
    transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World); // Normalise speed to time, relative to the game world to assure real-time movement, removing the issue of frame variability
    if (Vector3.Distance(transform.position, target.position) <= 0.4f) // To prevent sticking, swap to the next waypoint when the current target is less than 0.4 away
      getNextWaypoint();
    transform.LookAt(target);
  }
  void getNextWaypoint() {
    if (waypointIndex >= scrWaypoints.waypoints.Length - 1) { // If the enemy reaches the last waypoint
      Destroy(gameObject);
      return; // Stack overflow prevention to accommodate for the time it takes to destroy the gameObject
    }
    waypointIndex++;
    target = scrWaypoints.waypoints[waypointIndex]; // Swap to the next waypoint
  }
  void Die() {
    Destroy(gameObject);
  }
}
