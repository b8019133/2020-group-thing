﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class scrWaveSpawning : MonoBehaviour{
    public enum spawnState { spawning, waiting }; // State control for whether spawning should occur or not
    public spawnState state; // Set the initial state to spawning - for testing
    public Transform enemyPrefab1; // Ogre
    public Transform enemyPrefab2; // Boar
    public Transform enemyPrefab3; // Werewolf
    public Transform spawnPoint; // Can be expanded to encompass multiple spawn points easily in the future
    public int waveIndex = 0; // Set the starting wave to 0
    private float timeBetweenSpawns; // Utility value to accommodate a varying time between spawns, based on the type of enemy
    public bool waveInProgress = false;
    bool paytime = false;
    public List<GameObject> enemies = new List<GameObject>();
    private int spawns;
    public GameManager gm;
    void Start()
    {
        state = spawnState.waiting;
        spawns = 10;
        gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    void Update()
    {
        
       

        if (waveIndex <= 20) {
            if (state != spawnState.waiting) // If spawning should be occuring
            {
                StartCoroutine(SpawnWave()); // Spawn the wave
                paytime = true;
                state = spawnState.waiting; // Wait for button press

            }
        }
        else { 
            // Final wave has been reached - next map handling will go here if required
        }

        IEnumerator SpawnWave() {
            waveIndex++; // Increase the wave index, for the next wave to base off
            
            waveInProgress = true;
            for (int i = 0; i < spawns; i++) // Enemy number is directly linked to the wave number, wave 1 = 1 enemy, wave 5 = 5 enemies
            {
                SpawnEnemy(i); // Spawn the enemies
                yield return new WaitForSeconds(timeBetweenSpawns); // Wait a moment between spawns, to space out enemies
            }
            waveInProgress = false;
        }

        void SpawnEnemy(int waves)
        {
            if(waveIndex <= 3)
            {
                enemies.Add(Instantiate(enemyPrefab1, spawnPoint.position, Quaternion.identity).gameObject);
                timeBetweenSpawns = 0.5f;
                
                    
            }
            else if(waveIndex <= 8)
            {
                spawns = 30;
                if (waves <=25 )
                {
                    enemies.Add(Instantiate(enemyPrefab1, spawnPoint.position, Quaternion.identity).gameObject);
                    timeBetweenSpawns = 0.5f;
                }else
                {
                    enemies.Add(Instantiate(enemyPrefab2, spawnPoint.position, Quaternion.identity).gameObject);
                    timeBetweenSpawns = 1.0f;
                }

            }
            else if(waveIndex <= 15)
            {
                spawns = 40;
                if (waves <= 25)
                {
                    enemies.Add(Instantiate(enemyPrefab1, spawnPoint.position, Quaternion.identity).gameObject);
                    timeBetweenSpawns = 0.5f;
                }
                else if (waves <= 35)
                {
                    enemies.Add(Instantiate(enemyPrefab2, spawnPoint.position, Quaternion.identity).gameObject);
                    timeBetweenSpawns = 1.0f;
                }else
                {
                    enemies.Add(Instantiate(enemyPrefab3, spawnPoint.position, Quaternion.identity).gameObject);
                    timeBetweenSpawns = 0.25f;
                }
            }
            else if(waveIndex<= 20 )
            {
                spawns = 50;
                if (waves <= 35)
                {
                    enemies.Add(Instantiate(enemyPrefab1, spawnPoint.position, Quaternion.identity).gameObject);
                    timeBetweenSpawns = 0.5f;
                }
                else if (waves <= 42)
                {
                    enemies.Add(Instantiate(enemyPrefab2, spawnPoint.position, Quaternion.identity).gameObject);
                    timeBetweenSpawns = 1.0f;
                }
                else
                {
                    enemies.Add(Instantiate(enemyPrefab3, spawnPoint.position, Quaternion.identity).gameObject);
                    timeBetweenSpawns = 0.25f;
                }
            }
            

            /*
            if (waveIndex >= 15) // Spawn a werewolf every .25 seconds on waves 15 and above
            {
                enemies.Add(Instantiate(enemyPrefab3, spawnPoint.position, Quaternion.identity).gameObject);
                timeBetweenSpawns = 0.25f;

            }
            else if (waveIndex >= 10)// Spawn a boar every second on waves 10 to 14
            { 
                enemies.Add(Instantiate(enemyPrefab2, spawnPoint.position, Quaternion.identity).gameObject);
                timeBetweenSpawns = 1.0f;
            }
            else if (waveIndex >= 0)// Spawn an ogre every .5 seconds on waves 0 to 9
            { 
                enemies.Add(Instantiate(enemyPrefab1, spawnPoint.position, Quaternion.identity).gameObject);
                timeBetweenSpawns = 0.5f;
            }
            */

        }
    }

    public void ChangeStateSpawn()
    {
        state = spawnState.spawning;
    }

    public string GetCurrentState()
    {

        

        if ((state == spawnState.waiting) && (waveInProgress == false) && (enemies.Count == 0))
        {
            if ((paytime == true))
            {
                gm.ChangeMoney(waveIndex * 30);
                gm.game.waveNum += 1;
                paytime = false;
            }
            return "Waiting";
            
        }
        else
        {
            
            return "Spawning";
        }
    }

    public void CheckDestroy(GameObject remove)
    {
        enemies.Remove(remove);
    }

}
