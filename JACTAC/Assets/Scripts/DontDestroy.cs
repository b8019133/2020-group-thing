﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("GameController");

        if (objs.Length >= 2)
        {
            Destroy(objs[1]);
        }

        DontDestroyOnLoad(objs[0]);
    }
}
