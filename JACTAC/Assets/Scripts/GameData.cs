﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int waveNum;
    public int health;
    public int money;
    public int[] turrettype;
    //public static float[] turretz = new float[50];
    public GameData()
    {
        waveNum = 1;
        health = 20;
        money = 100;
        turrettype = new int[289];
    }
    public void SetEmpty()
    {
        for (int i = 0; i < 289; i++)
        {
            turrettype[i] = -1;
        }

    }
}
