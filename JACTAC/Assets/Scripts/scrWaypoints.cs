﻿using UnityEngine;

public class scrWaypoints : MonoBehaviour {
  public static Transform[] waypoints;
  void Awake() { // Construct a static list of all children that can be publicly accessed
    waypoints = new Transform[transform.childCount];
    for (int i = 0; i < waypoints.Length; i++) {
      waypoints[i] = transform.GetChild(i);
    }
  }
}
