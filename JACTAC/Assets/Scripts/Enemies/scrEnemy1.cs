﻿using UnityEngine;
using UnityEngine.UI;

public class scrEnemy1 : MonoBehaviour {
    public float speed = 1f; // Speed set to 10 - can be adjusted through Unity in runtime for demonstration
    public float startHealth = 100; // Health set to 100 - can be adjusted through Unity in runtime for demonstration
    private float health;
    [HideInInspector] public Transform target;
    private int waypointIndex = 0; // Index pointer for the waypoint targetting
    public Image healthBar;
    private GameObject gm;
    private GameObject spawner;
    [SerializeField] private AudioSource deathSound;

    void Awake() {
        target = scrWaypoints.waypoints[0]; // Set target to the first waypoint
        startHealth = gameObject.GetComponent<BaseEnemy>().health;
        spawner = GameObject.FindGameObjectWithTag("SpawnManager");
        gm = GameObject.FindGameObjectWithTag("GameController");
    }

    public void TakeDamage(float amount) {
        health -= amount;
        healthBar.fillAmount = health / startHealth;
        if (health <= 0)
            Die();
    }
    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World); // Normalise speed to time, relative to the game world to assure real-time movement, removing the issue of frame variability
        if (Vector3.Distance(transform.position, target.position) <= 0.4f) // To prevent sticking, swap to the next waypoint when the current target is less than 0.4 away
            getNextWaypoint();
        health = gameObject.GetComponent<BaseEnemy>().health;
        transform.LookAt(target);
    }
    void getNextWaypoint() {
        if (waypointIndex >= scrWaypoints.waypoints.Length - 1) { // If the enemy reaches the last waypoint
            gm.GetComponent<GameManager>().ChangeHealth(-1);
            spawner.SendMessage("CheckDestroy", gameObject);
            Destroy(gameObject);
            return; // Stack overflow prevention to accommodate for the time it takes to destroy the gameObject
        }
        waypointIndex++;
        target = scrWaypoints.waypoints[waypointIndex]; // Swap to the next waypoint
    }
    public void Die() {
        spawner.SendMessage("CheckDestroy", this.gameObject);
        deathSound.Play(0);
        Destroy(gameObject);
    }
}

  
    
  

 

