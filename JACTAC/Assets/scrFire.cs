﻿using UnityEngine;

public class scrFire : MonoBehaviour
{
  public GameObject fire;
  public GameManager gm;
  void Start()
  {
    gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
  }
  private void Update()
  {
    if (gm.game.health < 10)
    {
      fire.SetActive(true);
    }
  }
}
