﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour
{
    public Transform target;
    public EnemyDetection enemDet;
    public int fireSpeed;
    public int dmg;

    private void Start()
    {
        Destroy(gameObject, 7f*Time.deltaTime);
    }

    private void Update()
    {
        if (target != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, fireSpeed * Time.deltaTime);
        }
    }

  public void OnCollisionEnter(Collision other)
  {
    if (other.gameObject.tag == "Enemy")
    {
      Destroy(gameObject, 0f);

    }
  }
}