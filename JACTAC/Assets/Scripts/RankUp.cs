﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankUp : MonoBehaviour
{
    public int modelNumber = 0; //1 --> base. 2 --> rank 2. 3 --> rank 3
    public GameObject[] modelArray = { }; //index: 1 --> BASE. 2 --> UPGRADE1. 3 --> UPGRADE2.

    private void Start()
    {
        modelNumber = 0;
        ModelSwitch();
    }

    private void Update()
    {

    }

    public void ModelSwitch()
    {
        for (int x = 0; x < modelArray.Length; x++)
        {
            if (x == modelNumber)
            {
                modelArray[x].SetActive(true);
            }
            else
            {
                modelArray[x].SetActive(false);
            }
        }
        modelNumber += 1;
        if (modelNumber > modelArray.Length - 1)
        {
            modelNumber = modelArray.Length -1;
        }

    }
}
