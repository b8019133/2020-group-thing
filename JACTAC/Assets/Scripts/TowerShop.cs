﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShop : MonoBehaviour
{
    BuildManager buildManager;

    public void Start()
    {
        buildManager = BuildManager.instance;
    }

    public void PurchaseBallista()
    {
        buildManager.SetTowerToBuild(buildManager.towers[0], 0);
    }

    public void PurchaseCannon()
    {
        buildManager.SetTowerToBuild(buildManager.towers[1], 1);
    }

    public void PurchaseCatapult()
    {
        buildManager.SetTowerToBuild(buildManager.towers[2], 2);
    }
}
