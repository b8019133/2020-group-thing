﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameManager : MonoBehaviour
{
    enum GameStates { PLAYING, MAINMENU, SAVING, LOADING, STARTPLAYING, STARTMENU}
    GameStates current;
    bool changed = true;
    bool loaded = true;
    //public int money, health, wave;

    public GameData game = new GameData();

    void Awake()
    {
        game.SetEmpty();
    }

    void Update()
    {

        if (loaded == false)
        {
            for (int i = 0; i < 289; i++)
            {
                int x, z;
                x = i / 17;
                z = i % 17;
                
                GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<BuildManager>().LoadTurretData(x, z, game.turrettype[i]);

            }
            loaded = true;
        }



        if (changed == true)
        {

            if (current == GameStates.PLAYING)
            {
                changed = false;
            }
            if (current == GameStates.STARTPLAYING)
            {
        
                SceneManager.LoadScene("Level1");
                game = new GameData();
                current = GameStates.PLAYING;
            }
            if (current == GameStates.STARTMENU)
            {
                SceneManager.LoadScene("MainMenu");
                current = GameStates.MAINMENU;
            }
            if (current == GameStates.MAINMENU)
            {
                // load menu scene
                changed = false;
            }
            if (current == GameStates.SAVING)
            {
                BinaryFormatter bf = new BinaryFormatter();
                string path = Application.persistentDataPath + "/GameData.gd";
                //string path = "C:/Users/b8019133/Desktop/Saves/GameData.gd";
                FileStream filestream = new FileStream(path,FileMode.Create);
                bf.Serialize(filestream, game);
                filestream.Close();
                current = GameStates.STARTMENU;
            }
            if (current == GameStates.LOADING)
            {
                
                string path = Application.persistentDataPath + "/GameData.gd";
                //string path = "C:/Users/b8019133/Desktop/Saves/GameData.gd";
                if (File.Exists(path))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream filestream = new FileStream(path, FileMode.Open);
                    GameData oldgame = bf.Deserialize(filestream) as GameData;
                    filestream.Close();
                    game = oldgame;
                    game.money = oldgame.money;
                    game.health = oldgame.health;
                    game.waveNum = oldgame.waveNum;
                    SceneManager.LoadScene("Level1");
                    loaded = false;
                    
                    //Debug.Log(game.health);
                }
                else
                {
                    Debug.Log("error"); 
                }
                //start the game when loaded
                current = GameStates.PLAYING;              

            }
            
        }
    }

    public void PlayGame()
    {
        current = GameStates.PLAYING;
        changed = true;
    }
    public void StartGame()
    {
        current = GameStates.STARTPLAYING;
        changed = true;
    }
    public void OpenMenu()
    {
        current = GameStates.STARTMENU;
        changed = true;
    }
    public void GoToMenu()
    {
        current = GameStates.MAINMENU;
        changed = true;
    }
    public void SaveGame()
    {
        current = GameStates.SAVING;
        changed = true;
    }
    public void LoadGame()
    {
        current = GameStates.LOADING;
        changed = true;
    }

    public void ChangeHealth(int change)
    {
        game.health += change;
    }

    public void ChangeMoney(int change)
    {
        game.money += change;
    }
}
