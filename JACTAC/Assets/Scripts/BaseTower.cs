﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTower : MonoBehaviour
{
    public int dmg; //1 --> base. 2 --> rank 2. 3 --> rank 3
    public GameObject[] projectiles = {}; //index: 1 --> BalProj. 2 --> TrebProj. 3 --> Cannon Ball.
    public float fireRate = 1f, nextFire = 0.0f;
    public int health, cost;
    public Transform[] firePos;

    enum towerLevels {RANK1, RANK2, RANK3};
    towerLevels currentRank = towerLevels.RANK1;

    // Start is called before the first frame update
    void Start()
    {
        health = (int)gameObject.GetComponent<EnemyDetection>().health;

    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }

        if ((this.GetComponent<EnemyDetection>().target != null) && Time.time > nextFire)
        {
            for (int x = 0; x < firePos.Length; x++)
            {
                nextFire = Time.time + fireRate;
                //if one firePos
                GameObject projectile = Instantiate(projectiles[0], firePos[x].position, Quaternion.identity);
                BaseProjectile baseP = projectile.GetComponent<BaseProjectile>();
                baseP.target = this.GetComponent<EnemyDetection>().target;
                baseP.dmg = this.dmg;
            }

        }

    }
}
