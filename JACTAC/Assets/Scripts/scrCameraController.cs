﻿using UnityEngine;

public class scrCameraController : MonoBehaviour {
  [Header("Zoom")]
  [SerializeField] private float zoomSpeed = 120f; // Adjustable zoom speed
  private float zoomInLimit = 8f; // Limit zoom in to prevent clipping
  private float zoomOutLimit = 90f; // Limit zoom out to prevent the scene getting too small
  private float y; // Y value to check for changes in scroll wheel value
  private float xRotationLowerLimit = 10f; // Lower limit of rotation on the X axis to prevent clipping
  private float xRotationUpperLimit = 90f; // Upper limit of rotation on the X axis to prevent camera tumbling
  private float yRotation; // Utility value for Y rotation
  private float zRotation; // Utility value for X rotation
  [Space]
  [Header("Panning")]
  [SerializeField] private float leftLimit = -20f; // Prevent the player leaving the view of the scene
  [SerializeField] private float rightLimit = 100f; // ^^
  [SerializeField] private float upLimit = 80f; // ^^
  [SerializeField] private float downLimit = -40f; // ^^
  [SerializeField] private float panSpeed = 30f; // Adjustable pan speed
  [SerializeField] private float windowBorder = 20f; // Border around the inside of the window to register mouse-controlled panning
  private bool panSpeedToggle; // Utility bool to toggle the panning speed boost
  [Space]
  [Header("Mouse Rotation")]
  [SerializeField] private float rotateSpeed = 5f; // Rotation speed
  private Vector3 lastMousePosition; // Utility vector used for RMB camera controls
  private Quaternion initialRot; // Utility Quaternion used for camera rotation reset on RMB release

  void Start() { // Register utility values
    initialRot = transform.rotation;
    yRotation = transform.rotation.y;
    zRotation = transform.rotation.z;
    transform.rotation = Quaternion.Euler(transform.position.y, yRotation, zRotation); // Set initial camera rotation to match the zoom curve's
  }

  void Update() {
    if (Input.mousePosition.x >= 0 && Input.mousePosition.x <= Screen.width && Input.mousePosition.y >= 0 && Input.mousePosition.y <= Screen.height) { // Check that the mouse is within the screen radius
      #region Shift Boost
      if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.LeftShift)) // Toggle pan speed boost while holding 'Shift'
        panSpeedToggle = !panSpeedToggle;
      if (panSpeedToggle) // Boost pan speed when toggle is on
        panSpeed = 60f;
      else
        panSpeed = 30f;
      #endregion
      #region Scroll Zoom
      y = Input.mouseScrollDelta.y; // Set the scroll wheel value
      if (y >= 1 && transform.position.y > zoomInLimit) { // Zoom in with scroll wheel up
        initialRot = transform.rotation;
        transform.Translate(Vector3.down * zoomSpeed * Time.deltaTime, Space.World);
        if (transform.position.y > xRotationLowerLimit && transform.position.y < xRotationUpperLimit)
          transform.rotation = Quaternion.Euler(transform.position.y, yRotation, zRotation);
      }
      else if (y <= -1 && transform.position.y < zoomOutLimit){ // Zoom out with scroll wheel down
        initialRot = transform.rotation;
        transform.Translate(Vector3.up * zoomSpeed * Time.deltaTime, Space.World);
        if (transform.position.y > xRotationLowerLimit && transform.position.y < xRotationUpperLimit)
          transform.rotation = Quaternion.Euler(transform.position.y, yRotation, zRotation);
      }
      #endregion
      #region Key panning
      if ((Input.GetKey("w") || Input.mousePosition.y >= Screen.height - windowBorder) && transform.position.z < upLimit) // Move forward with 'W'
        transform.Translate(Vector3.forward * panSpeed * Time.deltaTime, Space.World);
      if ((Input.GetKey("s") || Input.mousePosition.y <= windowBorder) && transform.position.z > downLimit) // Move back with 'S'
        transform.Translate(Vector3.back * panSpeed * Time.deltaTime, Space.World);
      if ((Input.GetKey("a") || Input.mousePosition.x <= windowBorder) && transform.position.x > leftLimit) // Move left with 'A'
        transform.Translate(Vector3.left * panSpeed * Time.deltaTime, Space.World);
      if ((Input.GetKey("d") || Input.mousePosition.x >= Screen.width - windowBorder) && transform.position.x < rightLimit) // Move right with 'D'
        transform.Translate(Vector3.right * panSpeed * Time.deltaTime, Space.World);
      #endregion
      #region RMB Rotation
      if (Input.GetMouseButton(1)) { // Rotate the camera while RMB is held
        Vector3 mouseDelta;
        mouseDelta = Input.mousePosition - lastMousePosition;
        var rotation = Vector3.up * Time.deltaTime * rotateSpeed * mouseDelta.x;
        rotation += Vector3.left * Time.deltaTime * rotateSpeed * mouseDelta.y;
        transform.Rotate(rotation, Space.World);
        rotation = transform.rotation.eulerAngles;
        rotation.z = 0; // Lock Z rotation to 0 to prevent the camera toppling when rotated
        transform.rotation = Quaternion.Euler(rotation);
      }
      if (Input.GetMouseButtonUp(1)) // Reset the camera when RMB is released
        transform.rotation = Quaternion.Slerp(transform.rotation, initialRot, 0.5f * Time.time);
      lastMousePosition = Input.mousePosition;
      #endregion
    }
  }
}