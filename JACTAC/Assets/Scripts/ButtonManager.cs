﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public GameObject startWave, menu, resume, save, options, quit, mainMenu, spawnManager, wave, money, health, gameUI, instructions, mute, back, optionsMenu, muteSprite;
    public bool cansave = true;
    public bool muted = false;
    Button btn_startWave, btn_menu, btn_resume, btn_save, btn_options, btn_quit, btn_instructions, btn_mute, btn_back;
    private GameManager gm;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        gm.PlayGame();
        Time.timeScale = 0;
        btn_startWave = startWave.GetComponent<Button>();
        btn_menu = menu.GetComponent<Button>();
        btn_resume = resume.GetComponent<Button>();
        btn_save = save.GetComponent<Button>();
        btn_options = options.GetComponent<Button>();
        btn_quit = quit.GetComponent<Button>();
        btn_instructions = instructions.GetComponentInChildren<Button>();
        btn_mute = mute.GetComponent<Button>();
        btn_back = back.GetComponent<Button>();

        btn_startWave.onClick.AddListener(StartWave);
        btn_menu.onClick.AddListener(OpenMenu);
        btn_resume.onClick.AddListener(ResumeGame);
        btn_save.onClick.AddListener(SaveGame);
        btn_options.onClick.AddListener(OpenOptions);
        btn_quit.onClick.AddListener(QuitToMainMenu);
        btn_instructions.onClick.AddListener(CloseTutorial);
        btn_mute.onClick.AddListener(Mute);
        btn_back.onClick.AddListener(Back);


    }

    void Update()
    {
        if (spawnManager.GetComponent<scrWaveSpawning>().GetCurrentState() == "Waiting")
        {
            btn_startWave.GetComponentInChildren<Text>().text = "Start Wave";
            cansave = true;
            Time.timeScale = 1;
        }

        UpdateData();
    }

    void UpdateData()
    {
        wave.GetComponent<Text>().text = "Wave:  " + gm.game.waveNum;
        money.GetComponent<Text>().text = "x " + gm.game.money;
        health.GetComponent<Text>().text = "Health: "+ gm.game.health;
    }



    void StartWave()
    {
        if (spawnManager.GetComponent<scrWaveSpawning>().GetCurrentState() == "Waiting")
        {
            cansave = false;
            
            btn_startWave.GetComponentInChildren<Text>().text = "Speed Up";
            spawnManager.GetComponent<scrWaveSpawning>().ChangeStateSpawn();
        }
        else if (Time.timeScale == 1)
        {
            btn_startWave.GetComponentInChildren<Text>().text = "Slow Down";
            Time.timeScale = 2;
        }
        else if (Time.timeScale == 2)
        {
            Time.timeScale = 1;
            btn_startWave.GetComponentInChildren<Text>().text = "Speed Up";
        }
        
    }
    void OpenMenu()
    {
        Time.timeScale = 0;
        gameUI.SetActive(false);
        mainMenu.SetActive(true);
    }
    void ResumeGame()
    {
        Time.timeScale = 1;
        mainMenu.SetActive(false);
        gameUI.SetActive(true);
    }
    void SaveGame()
    {        
        Debug.Log("Saving");
        gm.SaveGame();
    }
    void OpenOptions()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }
    void Mute()
    {
        if (!muted)
        {
            muted = true;
            muteSprite.SetActive(true);
            AudioListener.volume = 0f;
        }
        else
        {
            muted = false;
            muteSprite.SetActive(false);
            AudioListener.volume = 1f;
        }
    }
    void Back()
    {
        optionsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
    void QuitToMainMenu()
    {
        Time.timeScale = 1;
        Debug.Log("Main Menu");
        gm.OpenMenu();
    }
    void CloseTutorial()
    {
        gameUI.SetActive(true);
        instructions.SetActive(false);
        Time.timeScale = 1;
    }
}
