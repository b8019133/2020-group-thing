﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseEnemy : MonoBehaviour
{
    public float health, dmg, speed;
    public int cost;
    public int levelMultiplier = 1;

    public float startHealth = 100; // Health set to 100 - can be adjusted through Unity in runtime for demonstration
    [HideInInspector] public Transform target;
    private int waypointIndex = 0; // Index pointer for the waypoint targetting
    public Image healthBar;
    private GameObject gm, spawner;
    [SerializeField] private AudioSource source;
    public AudioClip deathSound;
    bool dead = false;





    // Start is called before the first frame update
    void Awake()
    {
        health = (2 * levelMultiplier);
        speed = 10 + (levelMultiplier * 5);
        levelMultiplier = 1;

        target = scrWaypoints.waypoints[0]; // Set target to the first waypoint
        startHealth = health;
        spawner = GameObject.FindGameObjectWithTag("SpawnManager");
        gm = GameObject.FindGameObjectWithTag("GameController");

    }

    private void Update()
    {

        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World); // Normalise speed to time, relative to the game world to assure real-time movement, removing the issue of frame variability

        if (target != null)
        {
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = lookRotation.eulerAngles;
            transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }

        if (Vector3.Distance(transform.position, target.position) <= 0.4f) // To prevent sticking, swap to the next waypoint when the current target is less than 0.4 away
            getNextWaypoint();

        transform.LookAt(target);


        if (health <= 0)
        {
            if (!dead)
              Die();

        }
    }

        private void OnCollisionEnter(Collision collision)
        {
          if (collision.gameObject.tag == "End")
          {
            Destroy(gameObject);
          }
        
            if (collision.gameObject.tag == "Projectile")
            {
                dmg = collision.gameObject.GetComponent<BaseProjectile>().dmg;
                Debug.Log("Enemy Hit!");
                health -= dmg;
                Destroy(collision.gameObject,0f);
            
            }
        }


    void getNextWaypoint()
    {
        if (waypointIndex >= scrWaypoints.waypoints.Length - 1)
        { // If the enemy reaches the last waypoint
            gm.GetComponent<GameManager>().ChangeHealth(-1);
            spawner.SendMessage("CheckDestroy", gameObject);
            Destroy(gameObject);
            return; // Stack overflow prevention to accommodate for the time it takes to destroy the gameObject
        }
        waypointIndex++;
        target = scrWaypoints.waypoints[waypointIndex]; // Swap to the next waypoint
    }
    public void Die()
    {
        dead = true;
        spawner.SendMessage("CheckDestroy", this.gameObject);
        source.PlayOneShot(deathSound, .3f);
        GameObject gm = GameObject.FindGameObjectWithTag("GameController");
        GameManager gManage = gm.GetComponent<GameManager>();
        gManage.game.money += 10 * levelMultiplier;
        Destroy(gameObject, 2f);
  }
}
