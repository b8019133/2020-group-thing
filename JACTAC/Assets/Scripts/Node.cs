﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{
    [HideInInspector] public GameObject tower;
    private Renderer render;
    Vector3 posOffset = new Vector3(0f, 0f, 0f);
    Color trans = Color.red;
    BuildManager buildManager;

    // Start is called before the first frame update
    void Start()
    {
        render = gameObject.GetComponent<Renderer>();

        buildManager = BuildManager.instance;
        
        trans.a = 0f;
        render.material.color = trans;
    }
    
    void OnMouseEnter()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        if (buildManager.GetTowerToBuild() == null)
            return;

        trans.a = 0.4f;
        render.material.color = trans;
    }

    void OnMouseDown()
    {


        if(tower != null)
        {
            Debug.Log("Can't create there.");
            buildManager.SelectNode(this);
            return;
        }

        if (buildManager.GetTowerToBuild() == null)
            return;

        if (buildManager.CheckCosts(buildManager.GetTowerToBuild()) == true)
        {
            GameObject towerToBuild = buildManager.GetTowerToBuild();
            tower = (GameObject)Instantiate(towerToBuild, transform.position + posOffset, transform.rotation);
            buildManager.GetTurretData(transform.localPosition);
            buildManager.SetTowerToBuild(null, -1);

        }

       
    }

    public void BuildLoadedTower(GameObject type)
    {
        tower = (GameObject)Instantiate(type, transform.position + posOffset, transform.rotation);
    }

    void OnMouseExit()
    {
        trans.a = 0f;
        render.material.color = trans;
    }
}
